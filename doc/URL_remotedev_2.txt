GET https://localhost/ws/v2.0/Repositories/{repoId}/{schema}/{class}/{instanceId}


-- Repositories
https://remotedev.mcscad.com:446/ws/v2.5/Repositories
https://remotedev.mcscad.com:446/ws/v2.5/Repositories?$filter=Location+eq+'VHDDEVW2012S3.MCSCAD.local:BentleyExamples'

-- ECSchemaDef
https://remotedev.mcscad.com:446/ws/v2.5/Repositories/Bentley.PW--VHDDEVW2012R2S6.mcscad.local~3AProjectWise_Connect_Examples/MetaSchema/ECSchemaDef

-- ECClassDef
https://remotedev.mcscad.com:446/ws/v2.5/Repositories/Bentley.PW--VHDDEVW2012R2S6.mcscad.local~3AProjectWise_Connect_Examples/MetaSchema/ECClassDef

-- Project
https://remotedev.mcscad.com:446/ws/v2.5/Repositories/Bentley.PW--VHDDEVW2012R2S6.mcscad.local~3AProjectWise_Connect_Examples/PW_WSG/Project/
https://remotedev.mcscad.com:446/ws/v2.5/Repositories/Bentley.PW--VHDDEVW2012R2S6.mcscad.local~3AProjectWise_Connect_Examples/PW_WSG/Project?$top=10
https://remotedev.mcscad.com:446/ws/v2.5/Repositories/Bentley.PW--VHDDEVW2012R2S6.mcscad.local~3AProjectWise_Connect_Examples/PW_WSG/Project!poly?$top=100

-- Document
https://remotedev.mcscad.com:446/ws/v2.5/Repositories/Bentley.PW--VHDDEVW2012R2S6.mcscad.local~3AProjectWise_Connect_Examples/PW_WSG/Document?$top=10
https://remotedev.mcscad.com:446/ws/v2.5/Repositories/Bentley.PW--VHDDEVW2012R2S6.mcscad.local~3AProjectWise_Connect_Examples/PW_WSG/Document?$filter=Name+eq+'AllDGNLinkSets'
https://remotedev.mcscad.com:446/ws/v2.5/Repositories/Bentley.PW--VHDDEVW2012R2S6.mcscad.local~3AProjectWise_Connect_Examples/PW_WSG/Document?$filter=FileName+eq+'AllDGNLinkSets.qry'
https://remotedev.mcscad.com:446/ws/v2.5/Repositories/Bentley.PW--VHDDEVW2012R2S6.mcscad.local~3AProjectWise_Connect_Examples/PW_WSG/Document?$filter=ParentGuid+eq+'7d81200f-eeb7-4e79-abd3-6fe7b38cb607'
https://remotedev.mcscad.com:446/ws/v2.5/Repositories/Bentley.PW--VHDDEVW2012R2S6.mcscad.local~3AProjectWise_Connect_Examples/PW_WSG/Project/9ef61743-4c83-4154-a0c6-e05bdc97d6e7


-- TEST POST
https://remotedev.mcscad.com:446/ws/v2.1/Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/Project?$top=10

-- the project
https://remotedev.mcscad.com:446/ws/v2.1/Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/Project/8dd4c9cb-8ee4-453e-9a51-a9843fc94af9

-- all docs in project (Project Example 1, 8dd4c9cb-8ee4-453e-9a51-a9843fc94af9)
https://remotedev.mcscad.com:446/ws/v2.5/Repositories/Bentley.PW--VHDDEVW2012R2S6.mcscad.local~3AProjectWise_Connect_Examples/PW_WSG/Document?$filter=ParentGuid+eq+'7d81200f-eeb7-4e79-abd3-6fe7b38cb607'

-- the new doc
https://remotedev.mcscad.com:446/ws/v2.1/Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/Document/a1a3ecc6-1867-45ff-a1f2-cc433c2f10db
https://remotedev.mcscad.com:446/ws/v2.1/Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/Document?$filter=FileName+eq+'boris.txt'

*******************************************************

NEW DOC: POST: Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/document

JToken json = WsgHelper.CreateJson(documentToPost, relationship, documentParent);

-- documentToPost

{{
  "className": "Document",
  "schemaName": "PW_WSG",
  "instanceId": null,
  "properties": {
    "name": "\"boris.txt\""
  }
}}

-- relationship
{{
  "className": "DocumentParent",
  "schemaName": "PW_WSG",
  "instanceId": null,
  "direction": "forward"
}}

-- documentParent
{{
  "className": "Project",
  "schemaName": "PW_WSG",
  "instanceId": "8dd4c9cb-8ee4-453e-9a51-a9843fc94af9"
}}

+++++++

JToken json =

{{
  "instance": {
    "className": "Document",
    "schemaName": "PW_WSG",
    "instanceId": null,
    "properties": {
      "name": "\"boris.txt\""
    },
    "relationshipInstances": [
      {
        "className": "DocumentParent",
        "schemaName": "PW_WSG",
        "instanceId": null,
        "direction": "forward",
        "relatedInstance": {
          "className": "Project",
          "schemaName": "PW_WSG",
          "instanceId": "8dd4c9cb-8ee4-453e-9a51-a9843fc94af9"
        }
      }
    ]
  }
}}

NEW DOC
201 Created
{"changedInstance":{"change":"Created","instanceAfterChange":{"instanceId":"a1a3ecc6-1867-45ff-a1f2-cc433c2f10db","schemaName":"PW_WSG","className":"Document","properties":{"Name":"\"boris.txt\""},"relationshipInstances":[{"instanceId":"","schemaName":"PW_WSG","className":"DocumentParent","direction":"forward","properties":{},"relatedInstance":{"instanceId":"8dd4c9cb-8ee4-453e-9a51-a9843fc94af9","schemaName":"PW_WSG","className":"Project","properties":{}}}]}}}

https://remotedev.mcscad.com:446/ws/v2.1/Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/Document/a1a3ecc6-1867-45ff-a1f2-cc433c2f10db
https://remotedev.mcscad.com:446/ws/v2.1/Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/Document?$filter=FileName+eq+'boris.txt'

-- Project Example 1
parent https://remotedev.mcscad.com:446/ws/v2.1/Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/Document?$filter=ParentGuid+eq+'8dd4c9cb-8ee4-453e-9a51-a9843fc94af9'

*******************************************************

NEW PROJECT
POST to Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/Project

201 Created
{"changedInstance":{"change":"Created","instanceAfterChange":{"instanceId":"bd2eb802-886c-4d0d-bf99-099e86eb5cbc","schemaName":"PW_WSG","className":"Project","properties":{"Name":"\"Boris' Project\""},"relationshipInstances":[{"instanceId":"","schemaName":"PW_WSG","className":"ProjectParent","direction":"forward","properties":{},"relatedInstance":{"instanceId":"8dd4c9cb-8ee4-453e-9a51-a9843fc94af9","schemaName":"PW_WSG","className":"Project","properties":{}}}]}}}

parent: https://remotedev.mcscad.com:446/ws/v2.1/Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/Project/8dd4c9cb-8ee4-453e-9a51-a9843fc94af9
https://remotedev.mcscad.com:446/ws/v2.1/Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/Project/bd2eb802-886c-4d0d-bf99-099e86eb5cbc


*******************************************************

Source:
pw:\\PWSTCLC01.corp.xcelenergy.net:ProjectWise_NSP_Data\Documents TOP
\Xcel Folder Templates\
Substation Templates
\WBS Folder Template - Replace with Level 4\

Destination:
Logic, get here, example,  We need to find\ GUID's or translate.
pw:\\PWSTCLC01.corp.xcelenergy.net:ProjectWise_NSP_Data\Documents\Substation\Sub MN\ADA\

*******************************************************

// multipart post with file upload using WebApi 

using (var client = new HttpClient())
{
    using (var content = new MultipartFormDataContent())
    {
        var values = new[]
        {
            new KeyValuePair<string, string>("Foo", "Bar"),
            new KeyValuePair<string, string>("More", "Less"),
        };

        foreach (var keyValuePair in values)
        {
            content.Add(new StringContent(keyValuePair.Value), keyValuePair.Key);
        }

        var fileContent = new ByteArrayContent(System.IO.File.ReadAllBytes(fileName));
        fileContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
        {
            FileName = "Foo.txt"
        };
        content.Add(fileContent);

        var requestUri = "/api/action";
        var result = client.PostAsync(requestUri, content).Result;
    }
}

*******************************************************

http://stackoverflow.com/questions/30433387/httpclient-post-multipartformdatacontent-with-jtoken-and-string-returns-404-erro
private static HttpContent CreateMultipartContent(JToken json, Stream file, string fileName)
{
    MultipartContent content = new MultipartContent("form-data", Guid.NewGuid().ToString());

    StringContent jsonPart = new StringContent(json.ToString());
    jsonPart.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data");
    jsonPart.Headers.ContentType = new MediaTypeHeaderValue("application/json");

    StreamContent filePart = new StreamContent(file);
    filePart.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
    filePart.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data");
    filePart.Headers.ContentDisposition.FileName = fileName;

    content.Add(jsonPart);
    content.Add(filePart);

    return content;
}    

private static HttpContent CreateMultipartContent(JToken json, string markupText, string fileName)
{
    MultipartContent content = new MultipartContent("form-data", Guid.NewGuid().ToString());

    StringContent jsonPart = new StringContent(json.ToString());
    jsonPart.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data");
    jsonPart.Headers.ContentType = new MediaTypeHeaderValue("application/json");

    StringContent filePart = new StringContent(markupText);
    filePart.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
    filePart.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data");
    filePar`enter code here`t.Headers.ContentDisposition.FileName = fileName;

    content.Add(jsonPart);
    content.Add(filePart);

    return content;
}

*************************

POST: https://remotedev.mcscad.com:446/ws/v2.1/Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/Project

{
  "instance": {
    "className": "Project",
    "schemaName": "PW_WSG",
    "instanceId": null,
    "properties": {
      "Name": "A.0005521.014.001.015",
      "IsRichProject": true,
      "ComponentClassId": 1030,
      "EnvironmentId": 106
    },
    "relationshipInstances": [
      {
        "className": "ProjectParent",
        "schemaName": "PW_WSG",
        "instanceId": null,
        "direction": "forward",
        "relatedInstance": {
          "className": "Project",
          "schemaName": "PW_WSG",
          "instanceId": "f67bd937-58d7-4580-ab96-8fad42ca401f"
        }
      }
    ]
  }
}

RESPENSE
Created
{"changedInstance":{"change":"Created","instanceAfterChange":{"instanceId":"5e091ac1-d5d7-42ca-820d-9ee6025bf193","schemaName":"PW_WSG","className":"Project","properties":{"Name":"A.0005521.014.001.015","IsRichProject":true,"ComponentClassId":1030,"EnvironmentId":106},"relationshipInstances":[{"instanceId":"","schemaName":"PW_WSG","className":"ProjectParent","direction":"forward","properties":{},"relatedInstance":{"instanceId":"f67bd937-58d7-4580-ab96-8fad42ca401f","schemaName":"PW_WSG","className":"Project","properties":{}}}]}}}


PARENT:			https://remotedev.mcscad.com:446/ws/v2.1/Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/Project/f67bd937-58d7-4580-ab96-8fad42ca401f
PARENT CONTENT: https://remotedev.mcscad.com:446/ws/v2.1/Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/Project/?$filter=ParentGuid+eq+'f67bd937-58d7-4580-ab96-8fad42ca401f'

NEW PROJECT:	https://remotedev.mcscad.com:446/ws/v2.1/Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/Project/5e091ac1-d5d7-42ca-820d-9ee6025bf193

*******

{
  "instance": {
    "className": "Project",
    "schemaName": "PW_WSG",
    "instanceId": null,
    "properties": {
      "Name": "A.0005521.014.001.017",
      "IsRichProject": true,
      "ComponentClassId": 1027,
      "EnvironmentId": 101
    },
    "relationshipInstances": [
      {
        "className": "ProjectParent",
        "schemaName": "PW_WSG",
        "instanceId": null,
        "direction": "forward",
        "relatedInstance": {
          "className": "Project",
          "schemaName": "PW_WSG",
          "instanceId": "f67bd937-58d7-4580-ab96-8fad42ca401f"
        }
      }
    ]
  }
}


Created
{"changedInstance":{"change":"Created","instanceAfterChange":
{"instanceId":"85b6ae08-0c42-4cc8-8f11-386ae84dd5ef",
"schemaName":"PW_WSG","className":"Project","properties":{"Name":"A.0005521.014.001.017","IsRichProject":true,"ComponentClassId":1027,"EnvironmentId":101},"relationshipInstances":[{"instanceId":"","schemaName":"PW_WSG","className":"ProjectParent","direction":"forward","properties":{},"relatedInstance":{"instanceId":"f67bd937-58d7-4580-ab96-8fad42ca401f","schemaName":"PW_WSG","className":"Project","properties":{}}}]}}}

https://remotedev.mcscad.com:446/ws/v2.1/Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/Project/85b6ae08-0c42-4cc8-8f11-386ae84dd5ef


************************************************************************************************************************

CREATE POST: 
https://remotedev.mcscad.com:446/ws/v2.5/Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/Project

{
  "instance": {
    "className": "Project",
    "schemaName": "PW_WSG",
    "instanceId": null,
    "properties": {
      "Name": "A.0005521.014.001.016",
      "IsRichProject": true,
      "ComponentClassId": 1030,
      "EnvironmentId": 106
    },
    "relationshipInstances": [
      {
        "className": "ProjectParent",
        "schemaName": "PW_WSG",
        "instanceId": null,
        "direction": "forward",
        "relatedInstance": {
          "className": "Project",
          "schemaName": "PW_WSG",
          "instanceId": "f67bd937-58d7-4580-ab96-8fad42ca401f"
        }
      }
    ]
  }
}

RESPONSE:
Created
{"changedInstance":{"change":"Created","instanceAfterChange":{"instanceId":"8189aec2-b6d9-4cc1-9577-2aecf04518a2","schemaName":"PW_WSG","className":"Project","properties":{"Name":"A.0005521.014.001.016","IsRichProject":true,"ComponentClassId":1030,"EnvironmentId":106},"relationshipInstances":[{"instanceId":"","schemaName":"PW_WSG","className":"ProjectParent","direction":"forward","properties":{},"relatedInstance":{"instanceId":"f67bd937-58d7-4580-ab96-8fad42ca401f","schemaName":"PW_WSG","className":"Project","properties":{}}}]}}}


GET NEW PROJECT
https://remotedev.mcscad.com:446/ws/v2.1/Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/Project/8189aec2-b6d9-4cc1-9577-2aecf04518a2

************************************************************************************************************************

UPDATE POST:
https://remotedev.mcscad.com:446/ws/v2.5/Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/Project/8189aec2-b6d9-4cc1-9577-2aecf04518a2

{
  "instance": {
    "className": "Project",
    "schemaName": "PW_WSG",
    "instanceId": "8189aec2-b6d9-4cc1-9577-2aecf04518a2",
    "properties": {
      "Name": "A.0005521.014.001.016-UPD",
      "IsRichProject": true,
      "ComponentClassId": 1027,
      "EnvironmentId": 101
    },
    "relationshipInstances": [
      {
        "className": "ProjectParent",
        "schemaName": "PW_WSG",
        "instanceId": null,
        "direction": "forward",
        "relatedInstance": {
          "className": "Project",
          "schemaName": "PW_WSG",
          "instanceId": "f67bd937-58d7-4580-ab96-8fad42ca401f"
        }
      }
    ]
  }
}

************************************************************************************************************************

UPDATE POST:
https://remotedev.mcscad.com:446/ws/v2.5/Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/Project/645388f2-c079-4548-b26c-184ae5c0196a

{
  "instance": {
    "className": "Project",
    "schemaName": "PW_WSG",
    "instanceId": "645388f2-c079-4548-b26c-184ae5c0196a",
    "properties": {
      "Name": "A.0005521.014.001.111-UPD-100",
      "IsRichProject": true,
      "EnvironmentId": 102,
      "WorkflowId": 1,
      "ComponentClassId": 1030,
      "ComponentInstanceId": 8
    },
    "relationshipInstances": [
      {
        "className": "ProjectParent",
        "schemaName": "PW_WSG",
        "instanceId": null,
        "direction": "forward",
        "relatedInstance": {
          "className": "Project",
          "schemaName": "PW_WSG",
          "instanceId": "f67bd937-58d7-4580-ab96-8fad42ca401f"
        }
      },

      {
        instanceId: null,
		className: "ProjectProjectType",
        schemaName: "PW_WSG",
        direction: "forward",
		properties: {},

        relatedInstance: {
		  instanceId: 8,
          className: "PrType_1030_Building",
          schemaName: "PW_WSG_Dynamic",
          properties: {
            "PROJECT_Project_Name": "Boris"
          }
        }
      }

    ]
  }
}