-- Still have a distinct Max issue, getting some duplication
SELECT * FROM PW_SAP_WBS_INCOMING_VW
WHERE TARGET_ROOT_TPLNR Like 'SU-1041%'

-- SOURCE
SELECT * FROM PW_WSG_TEMPLATE_MAN

-- TARGET
SELECT * FROM PW_WSG_TARGET_JOB

---------------------------------------------------------------

SELECT * FROM [ProjectWise_Common].[dbo].[PW_WSG_TARGET_JOB]

DELETE FROM [dbo].[PW_WSG_TARGET_JOB] WHERE ID IN (18)

INSERT INTO PW_WSG_TARGET_JOB
  (PW_DATASOURCE, TEMPLATE_GUID, TEMPLATE_NAME, TARGET_GUID, TARGET_ROOT_TPLNR, TARGET_NAME, JOB_STATUS, [TimeStamp]) 
VALUES 
(
'PWSTCLC01.corp.xcelenergy.net:ProjectWise_NSP_Data',
'cd116521-5366-44d3-a605-e4b95de365b0',
'WBS Folder Template - Replace with Level 4',
'309e3b67-f7f5-4411-ad82-8b66de02f210',
'SU-1041',
'A.0005521.014.001.015',
'COMPLETE',
'2017-05-05 10:18:28'
)

-- The INSERT permission was denied on the object 'PW_WSG_TARGET_JOB', database 'ProjectWise_Common', schema 'dbo'.
-- connectionString="data source=SQLTCGO06\Instance1;Initial Catalog=ProjectWise_Common;User id=pwquery;Password=Xcel2929;"

-- pwadmin