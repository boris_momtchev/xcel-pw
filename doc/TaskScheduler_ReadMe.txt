What is the task scheduler?
Task Scheduler is a component of Microsoft Windows that provides the ability to schedule the launch of 
programs or scripts at pre-defined times or after specified time intervals: job scheduling (task scheduling).

How do I stop Task Scheduler?
Service name: Schedule. ...
Locate Task Scheduler observe his current status and open to make changes.
From General tab you can Start/Stop and change the Startup type of Task Scheduler. ...
Type regedit and press Enter.
Please navigate to HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\services\Schedule.
To Start Task Scheduler:

https://technet.microsoft.com/en-us/library/cc721931(v=ws.11).aspx

*** Start Task Scheduler

	* Task Scheduler MMC snap-in
	Applies To: Windows 7, Windows Server 2008 R2, Windows Server 2012, Windows Vista
	You can start the Task Scheduler MMC snap-in by using a single command from the command line or by using the Windows interface. 
	Task Scheduler can also be started by double-clicking the Taskschd.msc file in the %SYSTEMROOT%\System32 folder.

	* Windows interface
	To Run Task Scheduler using the Windows interface
	Click the Start button.
	Click Control Panel .
	Click System and Maintenance .
	Click Administrative Tools .
	Double-click Task Scheduler .

	* Command Line
	To Run Task Scheduler from the Command Line
	Open a command prompt. To open a command prompt, click Start , click All Programs , click Accessories , and then click Command Prompt .
	At the command prompt, type Taskschd.msc .

*** To create a task by using the Windows interface
https://technet.microsoft.com/en-us/library/cc748993(v=ws.11).aspx

	* To create a task by using a command line

	schtasks /Create [/S <system> [/U <username> [/P [<password>]]]]
		[/RU <username> [/RP <password>]] /SC <schedule> [/MO <modifier>] [/D <day>]
		[/M <months>] [/I <idletime>] /TN <taskname> /TR <taskrun> [/ST <starttime>]
		[/RI <interval>] [ {/ET <endtime> | /DU <duration>} [/K] [/XML <xmlfile>] [/V1]]
		[/SD <startdate>] [/ED <enddate>] [/IT] [/Z] [/F]

	* To create a task by using the Windows interface
		
	If Task Scheduler is not open, start Task Scheduler. For more information, see Start Task Scheduler.
	Find and click the task folder in the console tree that you want to create the task in. If you want to create the task in a new task folder, see Create a New Task Folder to create the folder.
	In the Actions Pane, click Create Task .
	On the General tab of the Create Task dialog box, enter a name for the task. Fill in or change any of the other properties on the General tab. For more information about these properties, see General Task Properties.
	On the Triggers tab of the Create Task dialog box, click the New� button to create a trigger for the task, and supply information about the trigger in the New Trigger dialog box. For more information about triggers, see Triggers.
	On the Actions tab of the Create Task dialog box, click the New� button to create an action for the task, and supply information about the action in the New Action dialog box. For more information about actions, see Actions.
	(Optional) On the Conditions tab of the Create Task dialog box, supply conditions for the task. For more information about the conditions, see Task Conditions.
	(Optional) On the Settings tab of the Create Task dialog box, change the settings for the task. For more information about the settings, see Task Settings.
	Click the OK button on the Create Task dialog box.


*** IMPORTANT CHANGE IN RESTSHARP VERSION 103 ***

In 103.0, JSON.NET was removed as a dependency. 

If this is still installed in your project and no other libraries depend on 
it you may remove it from your installed packages.

There is one breaking change: the default Json*Serializer* is no longer 
compatible with Json.NET. To use Json.NET for serialization, copy the code 
from https://github.com/restsharp/RestSharp/blob/86b31f9adf049d7fb821de8279154f41a17b36f7/RestSharp/Serializers/JsonSerializer.cs 
and register it with your client:

var client = new RestClient();
client.JsonSerializer = new YourCustomSerializer();

The default Json*Deserializer* is mostly compatible, but it does not support
all features which Json.NET has (like the ability to support a custom [JsonConverter]
by decorating a certain property with an attribute). If you need these features, you
must take care of the deserialization yourself to get it working.


