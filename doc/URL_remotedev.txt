https://remotedev.mcscad.com:446/ws/
https://remotedev.mcscad.com:446/ws/Pages/WsgExplorer.aspx

https://remotedev.mcscad.com:446/ws/Pages/documentation/index.html
https://remotedev.mcscad.com:446/ws/Pages/documentation/main_part.html#Downloading_file_attached_to_the_object_instance

EXPLORE:
https://remotedev.mcscad.com:446/ws/Pages/documentation/api_ref.html#6_7_5_Custom_query_parameters
https://remotedev.mcscad.com:446/ws/Pages/documentation/api_ref.html#6_7_7_Relationships_and_related_classes

*****

Bentley Web Services Gateway 02.05
Server Version: 02.05.02.03
Web API Version: v2.5
Copyright (c) 2017 Bentley Systems, Incorporated. All rights reserved.

// prj
https://remotedev.mcscad.com:446/ws/v2.5/Repositories/Bentley.PW--VHDDEVW2012R2S6.mcscad.local~3AProjectWise_Connect_Examples/PW_WSG/Project/f7b7f939-b28a-434c-914a-713842413cda

RICH PROJECT
https://remotedev.mcscad.com:446/ws/v2.5/Repositories/Bentley.PW--VHDDEVW2012R2S6.mcscad.local~3AProjectWise_Connect_Examples/PW_WSG/Project/a82f131a-8fc7-4e95-9695-5509bd8373a5
https://remotedev.mcscad.com:446/ws/v2.5/Repositories/Bentley.PW--VHDDEVW2012R2S6.mcscad.local~3AProjectWise_Connect_Examples/PW_WSG/Project?$filter=ParentGuid+eq+'a82f131a-8fc7-4e95-9695-5509bd8373a5'
https://remotedev.mcscad.com:446/ws/v2.5/Repositories/Bentley.PW--VHDDEVW2012R2S6.mcscad.local~3AProjectWise_Connect_Examples/PW_WSG/Document?$filter=ParentGuid+eq+'a82f131a-8fc7-4e95-9695-5509bd8373a5'


"TypeString": "Folder" vs Project


DOC in nested folder
https://remotedev.mcscad.com:446/ws/v2.5/Repositories/Bentley.PW--VHDDEVW2012R2S6.mcscad.local~3AProjectWise_Connect_Examples/PW_WSG/Project?$filter=ParentGuid+eq+'a82f131a-8fc7-4e95-9695-5509bd8373a5'
https://remotedev.mcscad.com:446/ws/v2.5/Repositories/Bentley.PW--VHDDEVW2012R2S6.mcscad.local~3AProjectWise_Connect_Examples/PW_WSG/Document?$filter=ParentGuid+eq+'a82f131a-8fc7-4e95-9695-5509bd8373a5'

https://remotedev.mcscad.com:446/ws/v2.5/Repositories/Bentley.PW--VHDDEVW2012R2S6.mcscad.local~3AProjectWise_Connect_Examples/PW_WSG/Document?$filter=ParentGuid+eq+'bbb31207-9b3c-4e25-9463-44ada65747ec'


*****

// doc meta and download
https://remotedev.mcscad.com:446/ws/v2.5/Repositories/Bentley.PW--VHDDEVW2012R2S6.mcscad.local~3AProjectWise_Connect_Examples/PW_WSG/Document/9e994e41-3ca2-4e32-ac8c-60aae893ade6
https://remotedev.mcscad.com:446/ws/v2.5/Repositories/Bentley.PW--VHDDEVW2012R2S6.mcscad.local~3AProjectWise_Connect_Examples/PW_WSG/Document/9e994e41-3ca2-4e32-ac8c-60aae893ade6/$file

// docs for project
https://remotedev.mcscad.com:446/ws/v2.5/Repositories/Bentley.PW--VHDDEVW2012R2S6.mcscad.local~3AProjectWise_Connect_Examples/PW_WSG/Document?$filter=ParentGuid+eq+'7d81200f-eeb7-4e79-abd3-6fe7b38cb607'

https://remotedev.mcscad.com:446/ws/v2.5/Repositories/Bentley.PW--VHDDEVW2012R2S6.mcscad.local~3AProjectWise_Connect_Examples/PW_WSG/Document?$filter=Project.Name+eq+'Transportation'
The query will return all instances without properties of the Document class and related instances with property Name of the class Project.

************

Xcel Version,

Bentley Web Services Gateway 02.00
Server Version: 02.00.01.23
Web API Version: v2.1
Copyright (c) 2015 Bentley Systems, Incorporated. All rights reserved.