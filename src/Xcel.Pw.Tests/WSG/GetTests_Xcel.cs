﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xcel.Pw.BO;

namespace Xcel.Pw.Tests.WSG
{
    [TestFixture]
    public class GetTests_Xcel
    {
        string _repo = "Bentley.PW--PWNSP.Corp.Xcelenergy.com~3AProjectWise_NSP_Data";

        [OneTimeSetUp]
        public void Init()
        {
            ServicePointManager.ServerCertificateValidationCallback +=
                (sender, certificate, chain, sslPolicyErrors) => true;
        }

        [Test]
        public async Task Get_Repositories()
        {
            var client = CreateXcelClient();
            var uri = "Repositories";

            var apiResult = (await client.ExecuteGetTaskAsync(new RestRequest(uri))).Content;
            var respObj = JsonConvert.DeserializeObject<BentleyResponseBase>(apiResult);

            string json = JsonConvert.SerializeObject(respObj, Formatting.Indented);
            Console.WriteLine(json);

            foreach (var item in respObj.instances)
                Console.WriteLine(item.instanceId);
        }

        [Test]
        public async Task Get_Repositories_Filtered()
        {
            var client = CreateXcelClient();
            var uri = "Repositories/?$filter=Location+eq+'PWNSP.Corp.Xcelenergy.com:ProjectWise_NSP_Data'";

            var apiResult = (await client.ExecuteGetTaskAsync(new RestRequest(uri))).Content;
            var respObj = JsonConvert.DeserializeObject<BentleyResponseBase>(apiResult);

            string json = JsonConvert.SerializeObject(respObj, Formatting.Indented);
            Console.WriteLine(json);

            foreach (var item in respObj.instances)
                Console.WriteLine(item.instanceId);
        }

        //------------------------------------------------------------------------

        [Test]
        public async Task Get_ECSchemaDef()
        {
            var client = CreateXcelClient();
            var uri = "Repositories/" + _repo + "/MetaSchema/ECSchemaDef";

            var apiResult = (await client.ExecuteGetTaskAsync(new RestRequest(uri))).Content;
            var respObj = JsonConvert.DeserializeObject<BentleyResponseBase>(apiResult);

            foreach (var item in respObj.instances)
                Console.WriteLine(item.instanceId);
        }

        [Test]
        public async Task Get_ECClassDef()
        {
            var client = CreateXcelClient();
            var uri = "Repositories/" + _repo + "/MetaSchema/ECClassDef";

            var apiResult = (await client.ExecuteGetTaskAsync(new RestRequest(uri))).Content;
            var respObj = JsonConvert.DeserializeObject<BentleyResponseBase>(apiResult);

            foreach (var item in respObj.instances)
                Console.WriteLine(item.instanceId);
        }

        //------------------------------------------------------------------------

        [Test]
        public async Task Get_Single_Project_Async()
        {
            var client = CreateXcelClient();
            var uri = "Repositories/" + _repo + "/PW_WSG/Project/f2a4d144-2cd6-4016-870d-2b2a970bf321";

            var apiResult = (await client.ExecuteGetTaskAsync(new RestRequest(uri))).Content;
            var respObj = JsonConvert.DeserializeObject<BentleyResponseBase>(apiResult);

            string json = JsonConvert.SerializeObject(respObj, Formatting.Indented);
            Console.WriteLine(json);

            foreach (var item in respObj.instances)
                Console.WriteLine(item.instanceId);
        }

        [Test]
        public void Get_Projects_Blocking()
        {
            var client = CreateXcelClient();
            // var uri = "Repositories/Bentley.PW--PWNSP.Corp.Xcelenergy.com~3AProjectWise_NSP_Data/PW_WSG/Project";
            var uri = "Repositories/" + _repo + "/PW_WSG/Project?$top=10";

            // var apiResult = (await client.ExecuteGetTaskAsync(new RestRequest(uri))).Content;
            var apiResult = client.Get(new RestRequest(uri)).Content;
            var respObj = JsonConvert.DeserializeObject<BentleyResponseBase>(apiResult);

            // string json = JsonConvert.SerializeObject(respObj, Formatting.Indented);
            // Console.WriteLine(json);

            foreach (var item in respObj.instances)
                Console.WriteLine(item.instanceId);
        }

        [Test]
        public void Get_Projects_Blocking_Filtered()
        {
            var client = CreateXcelClient();
            // var uri = "Repositories/Bentley.PW--PWSTCLC01.corp.xcelenergy.net~3AProjectWise_NSP_Data/PW_WSG/Project/?$filter=Name+eq+'CNST'";
            // https://pwstclc02.corp.xcelenergy.net/ws/v2.1/Repositories/Bentley.PW--PWSTCLC01.corp.xcelenergy.net~3AProjectWise_NSP_Data/PW_WSG/Project/?$top=10

            // var uri = "Repositories/Bentley.PW--PWSTCLC01.corp.xcelenergy.net~3AProjectWise_NSP_Data/PW_WSG/Project/?$filter=Name+eq+'Xcel Folder Templates'";
            // Project?$filter=Name+eq+'ECL'+and+EnvironmentId+eq+103
            var uri = "Repositories/" + _repo + "/PW_WSG/Project/?$filter=Name+eq+'ECL'";

            // var apiResult = (await client.ExecuteGetTaskAsync(new RestRequest(uri))).Content;
            var apiResult = client.Get(new RestRequest(uri)).Content;
            var respObj = JsonConvert.DeserializeObject<BentleyResponseBase>(apiResult);

            // string json = JsonConvert.SerializeObject(respObj, Formatting.Indented);
            // Console.WriteLine(json);

            foreach (var item in respObj.instances)
                Console.WriteLine(item.instanceId + " - " + item.properties["Name"]);
        }

        //------------------------------------------------------------------------

        [Test]
        public void Get_Documents_Filtered()
        {
            var client = CreateXcelClient();

            // PW_WSG/Document/?$top=10
            // PW_WSG/Document/?$filter=FileName+eq+'AllDGNLinkSets.qry'";
            // PW_WSG/Document/?$filter=Name+eq+'NSW_CrossarmComponentType.xls'";
            // PW_WSG/Document/?$filter=Name+like+'*.xls'";

            var uri = "Repositories/" + _repo + "/PW_WSG/Document/?$filter=Name+like+'*.xls'";

            // var apiResult = (await client.ExecuteGetTaskAsync(new RestRequest(uri))).Content;
            var apiResult = client.Get(new RestRequest(uri)).Content;
            var respObj = JsonConvert.DeserializeObject<BentleyResponseBase>(apiResult);

            // string json = JsonConvert.SerializeObject(respObj, Formatting.Indented);
            // Console.WriteLine(json);

            foreach (var item in respObj.instances)
                Console.WriteLine(item.properties["FileName"] + " - " + item.instanceId);
            // Console.WriteLine(item.instanceId);
        }

        //------------------------------------------------------------------------

        [Test]
        public async Task Get_Job_Template_Async()
        {
            var client = CreateXcelClient();
            var uri = "Repositories/" + _repo + "/PW_WSG/Project/f2a4d144-2cd6-4016-870d-2b2a970bf321";

            var apiResult = (await client.ExecuteGetTaskAsync(new RestRequest(uri))).Content;
            var respObj = JsonConvert.DeserializeObject<BentleyResponseBase>(apiResult);

            string json = JsonConvert.SerializeObject(respObj, Formatting.Indented);
            Console.WriteLine(json);

            foreach (var item in respObj.instances)
                Console.WriteLine(item.instanceId);
        }

        //------------------------------------------------------------------------

        [Test]
        public void Get_Repositories_TEST()
        {
            var client = new RestClient();
            client.BaseUrl = new Uri("https://pwscpgo20.corp.xcelenergy.com/ws/v2.1/");
            client.Authenticator = new NtlmAuthenticator(new NetworkCredential("227381", "Fac2018"));

            // client.Authenticator = new HttpBasicAuthenticator("tcorp\\227381", "Fac2018");
            // client.Authenticator = new WinAuthenticator();

            var request = new RestRequest();
            request.Resource = "Repositories";

            IRestResponse response = client.Execute(request);
            if (response.ErrorException != null)
            {
                Console.WriteLine(response.ErrorMessage);
                return;
            }

            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                Console.WriteLine(response.StatusCode);
                return;
            }

            var respObj = JsonConvert.DeserializeObject<BentleyResponseBase>(response.Content);
            string json = JsonConvert.SerializeObject(respObj, Formatting.Indented);
            Console.WriteLine(json);

            foreach (var item in respObj.instances)
                Console.WriteLine(item.instanceId);
        }

        // HELPERS
        RestClient CreateXcelClient()
        {
            //string baseUrl = "https://pwstclc02.corp.xcelenergy.net/ws/v2.1/";
            //string user = "tcorp\\227381";
            //string pass = "Dbg2017";

            //RestClient client = new RestClient(baseUrl);
            //client.Authenticator = new NtlmAuthenticator(new NetworkCredential(user, pass));

            /*
            string baseUrl = "https://remotedev.mcscad.com:446/ws/v2.5/";
            string user = "pwadmin";
            string pass = "pwadmin";
            */

            string baseUrl = "https://pwscpgo20.corp.xcelenergy.com/ws/v2.1/";
            //string user = "227381";
            //string pass = "Fac2018";

            RestClient client = new RestClient(baseUrl);
            client.Authenticator = new NtlmAuthenticator(new NetworkCredential("227381", "Fac2018"));
            // client.Authenticator = new HttpBasicAuthenticator(user, pass) as IAuthenticator;
            // client.Authenticator = new NtlmAuthenticator();
            // client.Authenticator = new WinAuthenticator();

            return client;
        }
    }
}
