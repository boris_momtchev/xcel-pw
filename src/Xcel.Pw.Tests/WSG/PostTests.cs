﻿using Newtonsoft.Json.Linq;
using NUnit.Framework;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Xcel.Pw.SVC;

namespace Xcel.Pw.Tests.WSG
{
    [TestFixture]
    public class PostTests
    {

        [Test]
        public void SimpleGet()
        {
            string baseUrl = "https://remotedev.mcscad.com:446/ws/v2.5/";
            string request = "Repositories/Bentley.PW--VHDDEVW2012R2S6.mcscad.local~3AProjectWise_Connect_Examples/PW_WSG/document?$top=10";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
                   Convert.ToBase64String(Encoding.ASCII.GetBytes(string.Format("{0}:{1}", "pwadmin", "pwadmin"))));

                // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "Your Oauth token");

                // var credentials = @"pwadmin:pwadmin";
                // credentials = Convert.ToBase64String(Encoding.UTF8.GetBytes(credentials));
                // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", credentials);

                HttpResponseMessage response = client.GetAsync(request).Result;
                if (response.StatusCode != HttpStatusCode.Created)
                {
                    Debug.WriteLine("error");
                }

                Debug.WriteLine(response.Content.ReadAsStringAsync().Result);
            }
        }

        [Test]
        public void PostProject()
        {
            JObject prjToPost = WsgHelper.CreateClassInstance("Project", "PW_WSG", null, new KeyValuePair<string, object>("name", "Boris' Project Third"));
            JObject relationship = WsgHelper.CreateRelationshipClassInstance("ProjectParent", "PW_WSG", null, "forward");
            JObject prjParent = WsgHelper.CreateClassInstance("Project", "PW_WSG", "8dd4c9cb-8ee4-453e-9a51-a9843fc94af9");

            // json part
            JToken json = WsgHelper.CreateJson(prjToPost, relationship, prjParent);

            string baseUrl = "https://remotedev.mcscad.com:446/ws/v2.1/";
            string request = "Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/Project";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
                    Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", "pwadmin", "pwadmin"))));

                using (var content = new MultipartFormDataContent())
                {
                    var jsonPart = new StringContent(json.ToString());
                    jsonPart.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data");
                    jsonPart.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    content.Add(jsonPart);

                    var result = client.PostAsync(request, content).Result;

                    Debug.WriteLine(result.StatusCode);
                    Debug.WriteLine(result.Content.ReadAsStringAsync().Result);
                }
            }
        }

        [Test]
        public void PostDocumentWithFileInOneRequest()
        {
            // Creating json that describes document with property 'name'
            JObject documentToPost = WsgHelper.CreateClassInstance("Document", "PW_WSG", null, new KeyValuePair<string, object>("name", "boris.txt"));

            // Specifying relationship that binds document to its parent (project)
            JObject relationship = WsgHelper.CreateRelationshipClassInstance("DocumentParent", "PW_WSG", null, "forward");

            // specifying document parent that is existing project with its id
            JObject documentParent = WsgHelper.CreateClassInstance("Project", "PW_WSG", "8dd4c9cb-8ee4-453e-9a51-a9843fc94af9");

            // json part
            JToken json = WsgHelper.CreateJson(documentToPost, relationship, documentParent);

            string baseUrl = "https://remotedev.mcscad.com:446/ws/v2.1/";
            string request = "Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/document";
            string fileName = "boris.txt";

            using (Stream file = new MemoryStream(Encoding.UTF8.GetBytes("boris' content...")))
            {
                // Posting document
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(baseUrl);

                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
                        Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", "pwadmin", "pwadmin"))));

                    HttpContent content = CreateMultipartContent(json, file, fileName);
                    HttpResponseMessage response = client.PostAsync(request, content).Result;
                    if (response.StatusCode != HttpStatusCode.Created)
                    {
                        Debug.WriteLine("error");
                    }

                    Debug.WriteLine(response.Content.ReadAsStringAsync().Result);
                }
            }
        }

        // PRIVATE
        static HttpContent CreateMultipartContent(JToken json, Stream file, string fileName)
        {
            MultipartContent content = new MultipartContent("form-data", Guid.NewGuid().ToString());

            StringContent jsonPart = new StringContent(json.ToString());
            jsonPart.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data");
            jsonPart.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            StreamContent filePart = new StreamContent(file);
            filePart.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            filePart.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data");
            filePart.Headers.ContentDisposition.FileName = fileName;

            content.Add(jsonPart);
            content.Add(filePart);

            return content;
        }

        RestClient CreateBentleyClient()
        {
            //string baseUrl = "https://pwstclc02.corp.xcelenergy.net/ws/v2.1/";
            //string user = "tcorp\\227381";
            //string pass = "NiCris2009";

            //RestClient client = new RestClient(baseUrl);
            //client.Authenticator = new NtlmAuthenticator(new NetworkCredential(user, pass));

            string baseUrl = "https://remotedev.mcscad.com:446/ws/v2.1/";
            string user = "pwadmin";
            string pass = "pwadmin";

            RestClient client = new RestClient(baseUrl);
            client.Authenticator = new HttpBasicAuthenticator(user, pass) as IAuthenticator;

            return client;
        }

    }
}
