﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Xcel.Pw.Tests.WSG
{
    [TestFixture]
    public class GetTests_Craigs
    {
        [Test]
        public async Task Get_Repositories()
        {
            var client = CreateBentleyClient();
            var uri = "Repositories";

            var apiResult = (await client.ExecuteGetTaskAsync(new RestRequest(uri))).Content;
            var respObj = JsonConvert.DeserializeObject<BentleyResponseBase>(apiResult);

            string json = JsonConvert.SerializeObject(respObj, Formatting.Indented);
            Console.WriteLine(json);

            foreach (var item in respObj.instances)
                Console.WriteLine(item.instanceId);
        }

        [Test]
        public async Task Get_Repositories_Filtered()
        {
            var client = CreateBentleyClient();
            var uri = "Repositories/?$filter=Location+eq+'VHDDEVW2012R2S6.mcscad.local:ProjectWise_Connect_Examples'";

            var apiResult = (await client.ExecuteGetTaskAsync(new RestRequest(uri))).Content;
            var respObj = JsonConvert.DeserializeObject<BentleyResponseBase>(apiResult);

            string json = JsonConvert.SerializeObject(respObj, Formatting.Indented);
            Console.WriteLine(json);

            foreach (var item in respObj.instances)
                Console.WriteLine(item.instanceId);
        }

        //------------------------------------------------------------------------

        [Test]
        public async Task Get_ECSchemaDef()
        {
            var client = CreateBentleyClient();
            var uri = "Repositories/Bentley.PW--VHDDEVW2012R2S6.mcscad.local~3AProjectWise_Connect_Examples/MetaSchema/ECSchemaDef";

            var apiResult = (await client.ExecuteGetTaskAsync(new RestRequest(uri))).Content;
            var respObj = JsonConvert.DeserializeObject<BentleyResponseBase>(apiResult);

            foreach (var item in respObj.instances)
                Console.WriteLine(item.instanceId);
        }

        [Test]
        public async Task Get_ECClassDef()
        {
            var client = CreateBentleyClient();
            var uri = "Repositories/Bentley.PW--VHDDEVW2012R2S6.mcscad.local~3AProjectWise_Connect_Examples/MetaSchema/ECClassDef";

            var apiResult = (await client.ExecuteGetTaskAsync(new RestRequest(uri))).Content;
            var respObj = JsonConvert.DeserializeObject<BentleyResponseBase>(apiResult);

            foreach (var item in respObj.instances)
                Console.WriteLine(item.instanceId);
        }

        //------------------------------------------------------------------------

        [Test]
        public async Task Get_Single_Project_Async()
        {
            var client = CreateBentleyClient();
            var uri = "Repositories/Bentley.PW--VHDDEVW2012R2S6.mcscad.local~3AProjectWise_Connect_Examples/PW_WSG/Project/9ef61743-4c83-4154-a0c6-e05bdc97d6e7";

            var apiResult = (await client.ExecuteGetTaskAsync(new RestRequest(uri))).Content;
            var respObj = JsonConvert.DeserializeObject<BentleyResponseBase>(apiResult);

            string json = JsonConvert.SerializeObject(respObj, Formatting.Indented);
            Console.WriteLine(json);

            foreach (var item in respObj.instances)
                Console.WriteLine(item.instanceId);
        }

        [Test]
        public void Get_Projects_Blocking()
        {
            var client = CreateBentleyClient();
            var uri = "Repositories/Bentley.PW--VHDDEVW2012R2S6.mcscad.local~3AProjectWise_Connect_Examples/PW_WSG/Project";

            // var apiResult = (await client.ExecuteGetTaskAsync(new RestRequest(uri))).Content;
            var apiResult = client.Get(new RestRequest(uri)).Content;
            var respObj = JsonConvert.DeserializeObject<BentleyResponseBase>(apiResult);

            // string json = JsonConvert.SerializeObject(respObj, Formatting.Indented);
            // Console.WriteLine(json);

            foreach (var item in respObj.instances)
                Console.WriteLine(item.instanceId);
        }

        [Test]
        public void Get_Projects_Blocking_Filtered()
        {
            var client = CreateBentleyClient();
            // var uri = "Repositories/Bentley.PW--PWSTCLC01.corp.xcelenergy.net~3AProjectWise_NSP_Data/PW_WSG/Project/?$filter=Name+eq+'CNST'";
            // https://pwstclc02.corp.xcelenergy.net/ws/v2.1/Repositories/Bentley.PW--PWSTCLC01.corp.xcelenergy.net~3AProjectWise_NSP_Data/PW_WSG/Project/?$top=10

            var uri = "Repositories/Bentley.PW--VHDDEVW2012R2S6.mcscad.local~3AProjectWise_Connect_Examples/PW_WSG/Project/?$filter=Name+eq+'dmsSystem'";

            // var apiResult = (await client.ExecuteGetTaskAsync(new RestRequest(uri))).Content;
            var apiResult = client.Get(new RestRequest(uri)).Content;
            var respObj = JsonConvert.DeserializeObject<BentleyResponseBase>(apiResult);

            // string json = JsonConvert.SerializeObject(respObj, Formatting.Indented);
            // Console.WriteLine(json);

            foreach (var item in respObj.instances)
                Console.WriteLine(item.instanceId + " - " + item.properties["Name"]);
        }

        //------------------------------------------------------------------------

        [Test]
        public void Get_Documents_Filtered()
        {
            var client = CreateBentleyClient();
            var uri = "Repositories/Bentley.PW--VHDDEVW2012R2S6.mcscad.local~3AProjectWise_Connect_Examples/PW_WSG/Document/?$filter=Name+like+'*.xls'";
            // var uri = "Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/Document/?$filter=FileName+eq+'AllDGNLinkSets.qry'";
            
            // var uri = "Repositories/Bentley.PW--PWSTCLC01.corp.xcelenergy.net~3AProjectWise_NSP_Data/PW_WSG/Document/?$filter=Name+eq+'NSW_CrossarmComponentType.xls'";
            // https://pwstclc02.corp.xcelenergy.net/ws/v2.1/Repositories/Bentley.PW--PWSTCLC01.corp.xcelenergy.net~3AProjectWise_NSP_Data/PW_WSG/Document/?$top=10
            // https://remotedev.mcscad.com:446/ws/v2.1/Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/Document?$top=10 (public)

            // var apiResult = (await client.ExecuteGetTaskAsync(new RestRequest(uri))).Content;
            var apiResult = client.Get(new RestRequest(uri)).Content;
            var respObj = JsonConvert.DeserializeObject<BentleyResponseBase>(apiResult);

            // string json = JsonConvert.SerializeObject(respObj, Formatting.Indented);
            // Console.WriteLine(json);

            foreach (var item in respObj.instances)
                Console.WriteLine(item.properties["FileName"] + " - " + item.instanceId);
            // Console.WriteLine(item.instanceId);
        }

        //------------------------------------------------------------------------

        [Test]
        public async Task Get_Job_Template_Async()
        {
            var client = CreateBentleyClient();
            var uri = "Repositories/Bentley.PW--VHDDEVW2012R2S6.mcscad.local~3AProjectWise_Connect_Examples/PW_WSG/Project/9ef61743-4c83-4154-a0c6-e05bdc97d6e7";

            var apiResult = (await client.ExecuteGetTaskAsync(new RestRequest(uri))).Content;
            var respObj = JsonConvert.DeserializeObject<BentleyResponseBase>(apiResult);

            string json = JsonConvert.SerializeObject(respObj, Formatting.Indented);
            Console.WriteLine(json);

            foreach (var item in respObj.instances)
                Console.WriteLine(item.instanceId);
        }

        //------------------------------------------------------------------------

        // HELPERS
        private RestClient CreateBentleyClient()
        {
            //string baseUrl = "https://pwstclc02.corp.xcelenergy.net/ws/v2.1/";
            //string user = "tcorp\\227381";
            //string pass = "NiCris2009";

            //RestClient client = new RestClient(baseUrl);
            //client.Authenticator = new NtlmAuthenticator(new NetworkCredential(user, pass));

            string baseUrl = "https://remotedev.mcscad.com:446/ws/v2.5/";
            string user = "pwadmin";
            string pass = "pwadmin";

            RestClient client = new RestClient(baseUrl);
            client.Authenticator = new HttpBasicAuthenticator(user, pass) as IAuthenticator;

            return client;
        }
    }
}
