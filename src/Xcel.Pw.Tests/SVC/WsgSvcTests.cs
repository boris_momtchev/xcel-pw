﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xcel.Pw.SVC;

namespace Xcel.Pw.Tests.SVC
{
    [TestFixture]
    public class WsgSvcTests
    {
        [Test]
        public void ProcessJobs_Test()
        {
            var svc = new WsgSvc();
            svc.ProcessJobs();
        }

        // ----------

        [Test]
        public void Test_GET_Repositories()
        {
            var svc = new WsgSvc();
            svc.Test_GET_Repositories();
        }

        [Test]
        public void Test_POST_Project()
        {
            var svc = new WsgSvc();
            svc.Test_POST_Project();
        }
    }
}
