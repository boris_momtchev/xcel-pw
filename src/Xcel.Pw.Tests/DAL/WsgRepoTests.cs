﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xcel.Pw.Tests.DAL
{
    [TestFixture]
    public class WsgRepoTests
    {
        [Test]
        public void Fetch_Jobs_Test()
        {
            var repo = new Xcel.Pw.DAL.WsgRepo();

            var jobs = repo.Fetch_Jobs();
            foreach (var item in jobs)
                Console.WriteLine(item.TARGET_ROOT_TPLNR + " - " + item.TARGET_NAME + " - " + item.SCHEMA + " - " + item.PARENT_FOLDER_NAME);
        }

        [Test]
        public void Fetch_Incomplete_INCOMING_Test()
        {
            var repo = new Xcel.Pw.DAL.WsgRepo();

            var INCOMING_VW = repo.Fetch_Incomplete_INCOMING();
            foreach (var item in INCOMING_VW)
                Console.WriteLine(item.TARGET_NAME + " - " + item.TEMPLATE + " - " + item.PARENT_FOLDER_NAME);
        }

        [Test]
        public void Fetch_Test()
        {
            var repo = new Xcel.Pw.DAL.WsgRepo();

            var INCOMING_VW = repo.Fetch_PW_SAP_WBS_INCOMING_VW();
            foreach (var item in INCOMING_VW)
                Console.WriteLine(item.TARGET_NAME + " - " + item.TEMPLATE);

            var TEMPLATE_MAN = repo.Fetch_PW_WSG_TEMPLATE_MAN();
            foreach (var item in TEMPLATE_MAN)
                Console.WriteLine(item.ID + " - " + item.TEMPLATE_NAME + " - " + item.SCHEMA + " - " + item.ATTRIBUTE_MATCH);

            var TARGET_JOB = repo.Fetch_PW_WSG_TARGET_JOB();
            foreach (var item in TEMPLATE_MAN)
                Console.WriteLine(item.ID + " - " + item.TEMPLATE_NAME);

        }
    }
}
