// The following C# example illustrates posting a document with file in one request.
private static string PostDocumentWithFileInOneRequest3()
{
    // Creating json that describes document with property 'name'
    JObject documentToPost = CreateClassInstance("Document", "{schema}", null, new KeyValuePair<string, object>("name", "MyTestDocument.txt"));

    // Specifying relationship that binds document to its parent (project)
    JObject relationship = CreateRelationshipClassInstance("DocumentParent", "{schema}", null, "forward");

    // specifying document parent that is existing project with its id
    JObject documentParent = CreateClassInstance("Project", "{schema}", "6aaf6a66-533e-41b8-9f6c-9a906bd06d62");

    JToken json = CreateJson(documentToPost, relationship, documentParent);
    Uri uri = new Uri(String.Format("http://localhost/v2.0/Repositories/pw--PW/{schema}/document", baseAddress));

    using (Stream file = new MemoryStream(Encoding.UTF8.GetBytes("My file contents....")))
    {
        // Posting document
        using (var client = new HttpClient())
        {
            var credentials = "admin:admin";
            credentials = Convert.ToBase64String(Encoding.UTF8.GetBytes(credentials));
            client.DefaultRequestHeaders.Add("Authorization", credentials);

            HttpContent content = CreateMultipartContent(json, uploadFile, fileName);

            HttpResponseMessage response = client.PostAsync(request, content).Result;
            if (response.StatusCode != HttpStatusCode.Created)
            {
                return "error"; // handle error
            }

            return response.Content.ReadAsStringAsync().Result;
        }
    }
}

private static string PostDocumentWithFileInOneRequest2()
{
    // Creating json that describes document with property 'name'
    JObject documentToPost = CreateClassInstance("Document", "{schema}", null, new KeyValuePair<string, object>("name", "MyTestDocument.txt"));

    // Specifying relationship that binds document to its parent (project)
    JObject relationship = CreateRelationshipClassInstance("DocumentParent", "{schema}", null, "forward");

    // specifying document parent that is existing project with its id
    JObject documentParent = CreateClassInstance("Project", "{schema}", "6aaf6a66-533e-41b8-9f6c-9a906bd06d62");

    // assemble
    JToken json = CreateJson(documentToPost, relationship, documentParent);

    string baseUrl = "https://remotedev.mcscad.com:446/ws/v2.1/";
    string request = "Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/document";
    string fileName = "boris.txt";

    using (Stream file = new MemoryStream(Encoding.UTF8.GetBytes("boris' contents...")))
    {
        // Posting document
        using (var client = new HttpClient())
        {
            client.BaseAddress = new Uri(baseUrl);
            var credentials = "admin:admin";
            credentials = Convert.ToBase64String(Encoding.UTF8.GetBytes(credentials));
            client.DefaultRequestHeaders.Add("Authorization", credentials);

            using (MultipartFormDataContent formContent = new MultipartFormDataContent())
            {
                HttpContent streamContent = new StreamContent(file, 4096);
                streamContent.Headers.ContentDisposition =
                    new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = fileName
                    };

                formContent.Add(streamContent);

                HttpResponseMessage response = client.PostAsync(request, formContent).Result;
                response.EnsureSuccessStatusCode();

                return response.Content.ReadAsStringAsync().Result;
            }
        }
    }
}


***************

// The following C# example illustrates posting a document with file in one request.
private static string PostDocumentWithFileInOneRequest()
{
    // Creating json that describes document with property 'name'
    JObject documentToPost = CreateClassInstance("Document", "{schema}", null, new KeyValuePair<string, object>("name", "boris_doc"));

    // Specifying relationship that binds document to its parent (project)
    JObject relationship = CreateRelationshipClassInstance("DocumentParent", "{schema}", null, "forward");

    // specifying document parent that is existing project with its id
    JObject documentParent = CreateClassInstance("Project", "{schema}", "f2d276bd-8dfe-43ec-b7aa-1bcc3177d7c6");

    JToken json = CreateJson(documentToPost, relationship, documentParent);
    try
    {
        string baseUrl = "https://remotedev.mcscad.com:446/ws/v2.1/";
        string request = "Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/document";
        string format = "application/octet-stream";

        string filename = "boris.txt";
        Stream stream = new MemoryStream(Encoding.UTF8.GetBytes("boris' content goes here"));

        var response = DoPostRequestAsync(request, baseUrl, json, stream, filename, format);
        Debug.WriteLine(response);
        return response;
    }
    catch (Exception ex)
    {
        Debug.WriteLine("UploadDocumentFile: " + ex.Message);
        return ex.Message;
    }
}

static string DoPostRequestAsync(string request, 
                                    string baseAddress,
                                    JToken json,
                                    Stream stream,
                                    string filename,
                                    string format = @"application/json",
                                    int bufferSize = 4096)
{
    using (HttpClient client = new HttpClient())
    {
        using (MultipartFormDataContent formContent = new MultipartFormDataContent())
        {
            client.BaseAddress = new Uri(baseAddress);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(format));

            var credentials = "pwadmin:pwadmin";
            credentials = Convert.ToBase64String(Encoding.UTF8.GetBytes(credentials));
            client.DefaultRequestHeaders.Add("Authorization", credentials);

            // *** json part
            var jsonPart = new StringContent(json.ToString());
            jsonPart.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data");
            jsonPart.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            formContent.Add(jsonPart);

            // *** file part
            var filePart = new StreamContent(stream);
            filePart.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            filePart.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data");
            filePart.Headers.ContentDisposition.FileName = filename;

            formContent.Add(filePart);

            // ---------

            HttpResponseMessage response = client.PostAsync(request, formContent).Result;
            response.EnsureSuccessStatusCode();

            string respContent = response.Content.ReadAsStringAsync().Result;
            return respContent;
        }
    }
}