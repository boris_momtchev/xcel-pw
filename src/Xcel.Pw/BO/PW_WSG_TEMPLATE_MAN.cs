﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xcel.Pw.BO
{
    public class PW_WSG_TEMPLATE_MAN
    {
        public int ID { get; set; }

        public string PW_DATASOURCE { get; set; }

        public Guid TEMPLATE_GUID { get; set; }
        public string TEMPLATE_NAME { get; set; }
        public string TEMPLATE { get; set; }

        public string TARGET_ROOT_NAME { get; set; }
        public string SCHEMA { get; set; }

        public string ATTRIBUTE_MATCH { get; set; }
    }
}
