﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xcel.Pw.BO
{
    public class PW_SAP_WBS_INCOMING_VW
    {
        public string TARGET_NAME { get; set; }
        public string TARGET_ROOT_TPLNR { get; set; }
        public string TEMPLATE { get; set; }
        public string ATTRIBUTE_VALUE { get; set; }
        public string PARENT_FOLDER_NAME { get; set; }

        // public string STATUS { get; set; }
        // public DateTime? LOAD_DATE_MAX { get; set; }
        // public string ATTRIBUTE { get; set; }
    }
}
