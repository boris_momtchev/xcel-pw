﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xcel.Pw.BO
{
    public class PW_WSG_TARGET_JOB
    {
        public int ID { get; set; }

        public string PW_DATASOURCE { get; set; }

        public Guid TEMPLATE_GUID { get; set; }
        public string TEMPLATE_NAME { get; set; }

        public Guid TARGET_GUID { get; set; }
        public string TARGET_ROOT_TPLNR { get; set; }
        public string TARGET_NAME { get; set; }

        // public string SAP_STATUS { get; set; }
        public string JOB_STATUS { get; set; }
        public DateTime? TimeStamp { get; set; }

        // DTO
        public string SCHEMA { get; set; }
        public string PW_DATASOURCE_ID { get; set; }

        public string PARENT_FOLDER_NAME { get; set; }
        public string TARGET_ROOT_NAME { get; set; }

        public string ATTRIBUTE_MATCH { get; set; }
        public string ATTRIBUTE_VALUE { get; set; }
    }
}

