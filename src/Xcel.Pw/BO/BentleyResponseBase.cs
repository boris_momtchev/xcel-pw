﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Xcel.Pw.BO
{
    public class BentleyResponseBase
    {
        public DtoInstance[] instances { get; set; }
    }

    public class DtoInstance
    {
        public string instanceId { get; set; }
        public string className { get; set; }
        public string schemaName { get; set; }
        public string eTag { get; set; }
        public Dictionary<string, object> properties { get; set; }

        public RelatedDtoInstance[] relationshipInstances { get; set; }

        public TPropertyType GetProperty<TPropertyType>([CallerMemberName] string property = "")
        {
            return properties != null && properties.ContainsKey(property) && properties[property] is TPropertyType
                ? (TPropertyType)properties[property]
                : default(TPropertyType);
        }

        public DtoInstance GetCustomAttributesInstance(string className = null)
        {
            if (relationshipInstances == null)
                return null;

            var relationshipInstance = relationshipInstances.FirstOrDefault(_ => _.className == className || string.IsNullOrEmpty(className));
            return relationshipInstance != null ? relationshipInstance.relatedInstance : null;
        }
    }

    public class RelatedDtoInstance : DtoInstance
    {
        public string direction { get; set; }
        public DtoInstance relatedInstance { get; set; }
    }

    public class BentleyResponseLite
    {
        public DtoInstanceLite[] instances { get; set; }
    }

    public class DtoInstanceLite
    {
        public string instanceId { get; set; }
        public PropertiesLite properties { get; set; }
    }

    public class PropertiesLite
    {
        public Guid? ParentGuid { get; set; }
    }

}
