﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xcel.Pw.BO;
using Xcel.Pw.DAL;

namespace Xcel.Pw.SVC
{
    public class WsgSvc
    {
        // FIELDS
        static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        readonly WsgRepo _repo;

        // C~tor
        public WsgSvc()
        {
            _repo = new WsgRepo();
        }

        // API
        public void ProcessJobs()
        {
            // 1. Fetch all incomplete incoming records (PW_SAP_WBS_INCOMING_VW)
            // 2. Create all new jobs formed from incomplete incoming + templates (PW_WSG_TEMPLATE_MAN)

            // TODO: Remove filter
            // var jobs = _repo.Fetch_Jobs();
            var jobs = _repo.Fetch_Jobs().Where(x => x.TARGET_ROOT_TPLNR.Contains("SU-1041"));

            if (!jobs.Any())
            {
                _logger.Info("ProcessJobs: No new jobs at this time.");
                return;
            }

            _logger.Info(string.Format("ProcessJobs: Constructed: {0} job(s)", jobs.Count()));

            // 3. Execute the jobs (PW_WSG_TARGET_JOB) against WSG API
            //    Jobs are executed recursively: all sub projects & files are copied from TEMPLATE to TARGET
            foreach (var job in jobs)
            {
                // 4. Get the template; Project/CD116521-5366-44D3-A605-E4B95DE365B0
                var templ = Fetch_Projects(job);

                _logger.Info(string.Format("ProcessJobs: Processing TEMPLATE: '{0}':{1}, TARGET: {2}", job.TEMPLATE_NAME, job.TEMPLATE_GUID, job.TARGET_NAME));
                Debug.WriteLine(templ.instances[0].instanceId);

                // 5. First level content; Project?$filter=ParentGuid+eq+'CD116521-5366-44D3-A605-E4B95DE365B0'
                // TODO: Switch to recursive for prj/docs
                var templContent = Fetch_Projects(job, getContent: true);

                // 6. Copy/POST: Template and first level content
                Create_Projects(job, templ, templContent);

                // 7. Write job state to DB
                Save_Job(job);
            }
        }


        // PRIVATE
        BentleyResponseBase Fetch_Projects(PW_WSG_TARGET_JOB job, bool getContent = false)
        {
            // GET /Repositories/{repoId}/{schema}/{class}/{instanceId}
            var client = CreateBentleyClient();

            // Repositories?$filter=Location+eq+'PWSTCLC01.corp.xcelenergy.net:ProjectWise_NSP_Data'
            // get the repoId: Bentley.PW--PWSTCLC01.corp.xcelenergy.net~3AProjectWise_NSP_Data
            var uri = string.Format("Repositories?$filter=Location+eq+'{0}'", job.PW_DATASOURCE);
            var apiResult = client.Get(new RestRequest(uri)).Content;
            var respObj = JsonConvert.DeserializeObject<BentleyResponseBase>(apiResult);
            string repoId = respObj.instances[0].instanceId;
            job.PW_DATASOURCE_ID = repoId;
            
            // template vs. children
            if (!getContent)
            {
                // Repositories/Bentley.PW--PWSTCLC01.corp.xcelenergy.net~3AProjectWise_NSP_Data/PW_WSG/Project/CD116521-5366-44D3-A605-E4B95DE365B0
                uri = string.Format("Repositories/{0}/{1}/Project/{2}", repoId, job.SCHEMA, job.TEMPLATE_GUID.ToString());
                apiResult = client.Get(new RestRequest(uri)).Content;
                respObj = JsonConvert.DeserializeObject<BentleyResponseBase>(apiResult);
            }
            else
            {
                // Repositories/Bentley.PW--PWSTCLC01.corp.xcelenergy.net~3AProjectWise_NSP_Data/PW_WSG/Project?$filter=ParentGuid+eq+'CD116521-5366-44D3-A605-E4B95DE365B0'
                uri = string.Format("Repositories/{0}/{1}/Project?$filter=ParentGuid+eq+'{2}'", repoId, job.SCHEMA, job.TEMPLATE_GUID.ToString());
                apiResult = client.Get(new RestRequest(uri)).Content;
                respObj = JsonConvert.DeserializeObject<BentleyResponseBase>(apiResult);
            }

            return respObj;
        }

        void Create_Projects(PW_WSG_TARGET_JOB job, BentleyResponseBase template, BentleyResponseBase templContent)
        {
            // TODO: Guard template, templContent?
            var templObj = template.instances[0];

            string className = templObj.className;                          // Project
            string schema = job.SCHEMA;                                     // PW_WSG
            // templObj.properties["Name"].ToString();                      // WBS Folder Template - Replace with Level 4

            // TEMPLATE:            Project/CD116521-5366-44D3-A605-E4B95DE365B0
            // TEMPLATE CONTENT:    Project?$filter=ParentGuid+eq+'CD116521-5366-44D3-A605-E4B95DE365B0'

            // PARENT TARGET:       Project/7d4aa16f-eb63-4b0e-ad61-5e134770c0a3    BRP/WBS Project Files
            string parentTargetGuid = Get_Parent_Target_Guid(job, className);

            // HttpClient Setup
            string baseUrl = "https://pwstclc02.corp.xcelenergy.net/ws/v2.1/";
            string request = "Repositories/Bentley.PW--PWSTCLC01.corp.xcelenergy.net~3AProjectWise_NSP_Data/PW_WSG/Project";
            var handler = new HttpClientHandler();
            handler.Credentials = new NetworkCredential("tcorp\\227381", "NiCris2009");

            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(baseUrl);

                // ----------------------------------------------------------------------

                // TARGET: We have to create; BRP/WBS Project Files/A.0005521.014.001.015
                string targetGuid = string.Empty;
                string targetName = job.TARGET_NAME;

                // TODO: Set Description, IsRichProject (true only for parent), "ComponentClassId": 1044 (parent only, otherwise 0;default)
                // TODO: "ComponentInstanceId": 3 if I get this back; put in log
                var properties = new KeyValuePair<string, object>[]
                    {
                        new KeyValuePair<string, object>("Name", targetName),
                        new KeyValuePair<string, object>("IsRichProject", true),
                        new KeyValuePair<string, object>("ComponentClassId", 1044),
                        new KeyValuePair<string, object>("EnvironmentId", 103)
                        // new KeyValuePair<string, object>("TypeString", "Project")
                    };
                JObject jsonProject = WsgHelper.CreateClassInstance(className, job.SCHEMA, null, properties);

                // JObject jsonProject = WsgHelper.CreateClassInstance(className, job.SCHEMA, null, new KeyValuePair<string, object>("Name", targetName));
                JObject jsonRelationship = WsgHelper.CreateRelationshipClassInstance("ProjectParent", schema, null, "forward");
                JObject jsonParent = WsgHelper.CreateClassInstance(className, schema, parentTargetGuid);
                JToken json = WsgHelper.CreateJson(jsonProject, jsonRelationship, jsonParent);

                _logger.Debug("Create_Projects: targetName: " + targetName);
                _logger.Debug("Create_Projects: parentTargetGuid: " + parentTargetGuid);
                _logger.Debug("Create_Projects: TARGET Json: " + json.ToString());

                using (var content = new MultipartFormDataContent())
                {
                    var jsonPart = new StringContent(json.ToString());
                    jsonPart.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data");
                    jsonPart.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    content.Add(jsonPart);

                    var result = client.PostAsync(request, content).Result;

                    // Debug.WriteLine(result.StatusCode);
                    // Debug.WriteLine(result.Content.ReadAsStringAsync().Result);
                    _logger.Info(string.Format("Create_Projects: POST TARGET: {0}, StatusCode: {1}, Result: {2}", 
                        targetName, result.StatusCode, result.Content.ReadAsStringAsync().Result));

                    if (result.StatusCode.ToString().ToLower() != "created")
                    {
                        _logger.Warn("Unable to CREATE TARGET. Current Job: FAILED");
                        job.JOB_STATUS = "FAILED";
                        return;
                    }

                    // Parse to dynamic; extract targetGuid
                    var dynObj = JsonConvert.DeserializeObject<dynamic>(result.Content.ReadAsStringAsync().Result);
                    targetGuid = dynObj["changedInstance"]["instanceAfterChange"]["instanceId"];
                    _logger.Info(string.Format("Create_Projects: TARGET GUID: {0}", targetGuid));

                    job.TARGET_GUID = new Guid(targetGuid);
                }

                // ----------------------------------------------------------------------

                // Copy all TEMPLATE CONTENT to TARGET/Job:   
                foreach (var tc in templContent.instances)
                {
                    string tcName = tc.properties["Name"].ToString();

                    jsonProject = WsgHelper.CreateClassInstance(className, job.SCHEMA, null, new KeyValuePair<string, object>("Name", tcName));
                    jsonRelationship = WsgHelper.CreateRelationshipClassInstance("ProjectParent", schema, null, "forward");
                    jsonParent = WsgHelper.CreateClassInstance(className, schema, targetGuid);
                    
                    // TODO: Add more properties / custom attr? This is just the name so far..
                    json = WsgHelper.CreateJson(jsonProject, jsonRelationship, jsonParent);

                    using (var content = new MultipartFormDataContent())
                    {
                        var jsonPart = new StringContent(json.ToString());
                        jsonPart.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data");
                        jsonPart.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                        content.Add(jsonPart);

                        var result = client.PostAsync(request, content).Result;

                        // Debug.WriteLine(result.StatusCode);
                        // Debug.WriteLine(result.Content.ReadAsStringAsync().Result);
                        _logger.Info(string.Format("Create_Projects: POST CONTENT: {0}, StatusCode: {1}, Result: {2}",
                            tcName, result.StatusCode, result.Content.ReadAsStringAsync().Result));

                        if (result.StatusCode.ToString().ToLower() != "created")
                        {
                            _logger.Warn("Unable to CREATE CONTENT. Current Job: FAILED");
                            job.JOB_STATUS = "FAILED";
                            return;
                        }
                    }
                }
            }
        }

        void Save_Job(PW_WSG_TARGET_JOB job)
        {
            _repo.Save_Job(job);
        }

        // HELPERS
        string Get_Parent_Target_Guid(PW_WSG_TARGET_JOB job, string className)
        {
            // We're looking for: Project/7d4aa16f-eb63-4b0e-ad61-5e134770c0a3    BRP/WBS Project Files
            var parentGuid = string.Empty;
            var targetGuid = string.Empty;

            var client = CreateBentleyClient();

            var repoId = job.PW_DATASOURCE_ID;              // "Bentley.PW--PWSTCLC01.corp.xcelenergy.net~3AProjectWise_NSP_Data";
            var schema = job.SCHEMA;                        // "PW_WSG";
            var parentFolder = job.PARENT_FOLDER_NAME;      // "BRP";
            var attrMatch = job.ATTRIBUTE_MATCH;            // "FUNCTIONAL_LOCATION";
            var attrVal = job.ATTRIBUTE_VALUE;              // "SU-1041";
            var targetRootName = job.TARGET_ROOT_NAME;      // "WBS Project Files";

            // Repositories/Bentley.PW--PWSTCLC01.corp.xcelenergy.net~3AProjectWise_NSP_Data/PW_WSG/Project/?$filter=Name+eq+'BRP'
            var uri = string.Format("Repositories/{0}/{1}/{2}/?$filter=name+eq+'{3}'", repoId, schema, className, parentFolder);
            var apiResult = client.Get(new RestRequest(uri)).Content;
            var respObj = JsonConvert.DeserializeObject<BentleyResponseBase>(apiResult);

            foreach (var ins in respObj.instances)
            {
                // Repositories/Bentley.PW--PWSTCLC01.corp.xcelenergy.net~3AProjectWise_NSP_Data/PW_WSG/Project/113a8a33-33e7-4e90-8a83-2266d7662e7b?$select=ProjectType!poly.*,*
                uri = string.Format("Repositories/{0}/{1}/{2}/{3}/?$select=ProjectType!poly.*,*", repoId, schema, className, ins.instanceId);
                apiResult = client.Get(new RestRequest(uri)).Content;
                respObj = JsonConvert.DeserializeObject<BentleyResponseBase>(apiResult);

                var relIns = respObj.instances[0].relationshipInstances[0];
                if (relIns == null)
                    continue;

                var relInsRec = relIns.relatedInstance;
                if (relInsRec == null)
                    continue;

                // Do we have a match on FUNCTIONAL_LOCATION=SU-1041?
                string attrMatchValue = string.Empty;
                if (relInsRec.properties.ContainsKey(attrMatch))
                    attrMatchValue = relInsRec.properties[attrMatch].ToString();    // "FUNCTIONAL_LOCATION": "SU-1041",
                else
                    continue;

                if (attrMatchValue == attrVal)
                {
                    // PARENT TARGET (BRP: 113a8a33-33e7-4e90-8a83-2266d7662e7b) by Name, and "FUNCTIONAL_LOCATION": "SU-1041",
                    parentGuid = ins.instanceId;

                    // Grab TARGET's CONTENT
                    // Repositories/Bentley.PW--PWSTCLC01.corp.xcelenergy.net~3AProjectWise_NSP_Data/PW_WSG/Project?$filter=ParentGuid+eq+'113a8a33-33e7-4e90-8a83-2266d7662e7b'
                    // uri = string.Format("Repositories/{0}/{1}/{2}?$filter=ParentGuid+eq+'{3}'", repoId, schema, className, parentGuid);

                    // TARGET 
                    // Project?$filter=ParentGuid+eq+'113a8a33-33e7-4e90-8a83-2266d7662e7b'+and+Name+eq+'BRP'
                    uri = string.Format("Repositories/{0}/{1}/{2}?$filter=ParentGuid+eq+'{3}'+and+Name+eq+'{4}'", repoId, schema, className, parentGuid, targetRootName);
                    apiResult = client.Get(new RestRequest(uri)).Content;
                    respObj = JsonConvert.DeserializeObject<BentleyResponseBase>(apiResult);
                    targetGuid = respObj.instances[0].instanceId;

                    return targetGuid;
                }
            }

            return targetGuid;
        }

        RestClient CreateBentleyClient()
        {
            string baseUrl = "https://pwstclc02.corp.xcelenergy.net/ws/v2.1/";
            string user = "tcorp\\227381";
            string pass = "NiCris2009";

            RestClient client = new RestClient(baseUrl);
            client.Authenticator = new NtlmAuthenticator(new NetworkCredential(user, pass));

            //string baseUrl = "https://remotedev.mcscad.com:446/ws/v2.1/";
            //string user = "pwadmin";
            //string pass = "pwadmin";

            //RestClient client = new RestClient(baseUrl);
            //client.Authenticator = new HttpBasicAuthenticator(user, pass) as IAuthenticator;

            return client;
        }

        // TESTS
        public void Test_GET_Repositories()
        {
            var client = CreateBentleyClient();
            var uri = "Repositories";

            var apiResult = client.Get(new RestRequest(uri)).Content;
            var respObj = JsonConvert.DeserializeObject<BentleyResponseBase>(apiResult);

            string json = JsonConvert.SerializeObject(respObj, Formatting.Indented);
            _logger.Info(json);

            foreach (var item in respObj.instances)
                _logger.Info(item.instanceId);
        }

        public void Test_POST_Project()
        {
            // Repos:
            // https://remotedev.mcscad.com:446/ws/v2.5/Repositories
            // https://remotedev.mcscad.com:446/ws/v2.5/Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/Project?$top=10

            // Find by Name | by IsRichProject=true
            // https://remotedev.mcscad.com:446/ws/v2.5/Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/Project/?$name+eq+'Boris'
            // https://remotedev.mcscad.com:446/ws/v2.1/Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/Project/?$filter=IsRichProject+eq+true

            // TARGET
            // https://remotedev.mcscad.com:446/ws/v2.5/Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/Project/f67bd937-58d7-4580-ab96-8fad42ca401f
            // https://remotedev.mcscad.com:446/ws/v2.5/Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/Project/?$filter=ParentGuid+eq+'f67bd937-58d7-4580-ab96-8fad42ca401f'

            string baseUrl = "https://remotedev.mcscad.com:446/ws/v2.1/";

            // CREATE vs. UPDATE
            // string request = "Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/Project";
            string request = "Repositories/Bentley.PW--VHDDEVW2012S3.MCSCAD.local~3ABentleyExamples/PW_WSG/Project/645388f2-c079-4548-b26c-184ae5c0196a";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
                   Convert.ToBase64String(Encoding.ASCII.GetBytes(string.Format("{0}:{1}", "pwadmin", "pwadmin"))));

                var properties = new KeyValuePair<string, object>[]
                    {
                        // works
                        new KeyValuePair<string, object>("Name", "A.0005521.014.001.111-UPD-3"),
                        new KeyValuePair<string, object>("IsRichProject", true),
                        new KeyValuePair<string, object>("EnvironmentId", 102),
                        new KeyValuePair<string, object>("WorkflowId", 1),

                        // nope
                        new KeyValuePair<string, object>("ComponentClassId", 1030),
                        new KeyValuePair<string, object>("ComponentInstanceId", 8)
                    };

                // CREATE vs. UPDATE
                // JObject jsonProject = WsgHelper.CreateClassInstance("Project", "PW_WSG", null, properties);
                JObject jsonProject = WsgHelper.CreateClassInstance("Project", "PW_WSG", "645388f2-c079-4548-b26c-184ae5c0196a", properties);

                // Relationship w/ parent
                JObject jsonRelationship = WsgHelper.CreateRelationshipClassInstance("ProjectParent", "PW_WSG", null, "forward");
                JObject jsonParent = WsgHelper.CreateClassInstance("Project", "PW_WSG", "f67bd937-58d7-4580-ab96-8fad42ca401f");



                // Final JSON
                // JToken json = WsgHelper.CreateJson(jsonProject, jsonRelationship, jsonParent);
                JToken json = json_to_post;

                Debug.WriteLine(json);

                using (var content = new MultipartFormDataContent())
                {
                    var jsonPart = new StringContent(json.ToString());
                    jsonPart.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data");
                    jsonPart.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    content.Add(jsonPart);

                    var result = client.PostAsync(request, content).Result;

                    Debug.WriteLine(result.StatusCode);
                    Debug.WriteLine(result.Content.ReadAsStringAsync().Result);
                }
            }
        }


        const string json_to_post =
@"
{
  ""instance"": {
    ""className"": ""Project"",
    ""schemaName"": ""PW_WSG"",
    ""instanceId"": ""645388f2-c079-4548-b26c-184ae5c0196a"",
    ""properties"": {
      ""Name"": ""A.0005521.014.001.111-UPD-100"",
      ""IsRichProject"": true,
      ""EnvironmentId"": 102,
      ""WorkflowId"": 1,
      ""ComponentClassId"": 1030,
      ""ComponentInstanceId"": 8
    },
    ""relationshipInstances"": [
      {
        ""className"": ""ProjectParent"",
        ""schemaName"": ""PW_WSG"",
        ""instanceId"": null,
        ""direction"": ""forward"",
        ""relatedInstance"": {
          ""className"": ""Project"",
          ""schemaName"": ""PW_WSG"",
          ""instanceId"": ""f67bd937-58d7-4580-ab96-8fad42ca401f""
        }
      },

      {
		""className"": ""ProjectProjectType"",
        ""schemaName"": ""PW_WSG"",
        ""direction"": ""forward"",

        ""relatedInstance"": {
		  ""instanceId"": 8,
          ""className"": ""PrType_1030_Building"",
          ""schemaName"": ""PW_WSG_Dynamic"",
          ""properties"": {
            ""PROJECT_Project_Name"": ""Boris""
          }
        }
      }

    ]
  }
}
";


    }
}
