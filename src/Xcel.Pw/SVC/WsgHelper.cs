﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xcel.Pw.SVC
{
    public class WsgHelper
    {
        public static JObject CreateClassInstance(string className, string schemaName, string id, params KeyValuePair<string, object>[] properties)
        {
            var newClass = new JObject();
            newClass.Add("className", className);
            newClass.Add("schemaName", schemaName);
            newClass.Add("instanceId", id);

            if (properties.Length > 0)
            {
                var propertiesObject = new JObject();
                foreach (KeyValuePair<string, object> property in properties)
                {
                    // TODO: Check this
                    if (property.Key.ToLower() == "name")
                        propertiesObject.Add(property.Key, property.Value.ToString());

                    else if (property.Key.ToLower() == "isrichproject")
                        propertiesObject.Add(property.Key, (bool)property.Value);

                    else if (property.Key.ToLower() == "componentclassid" || property.Key.ToLower() == "componentinstanceid" || property.Key.ToLower() == "workflowid")
                        propertiesObject.Add(property.Key, (int)property.Value);

                    else if (property.Key.ToLower() == "environmentid")
                        propertiesObject.Add(property.Key, (int)property.Value);

                    else
                        propertiesObject.Add(property.Key, JsonConvert.SerializeObject(property.Value));
                }

                newClass.Add("properties", propertiesObject);
            }
            return newClass;
        }

        public static JObject CreateRelationshipClassInstance(string className, string schemaName, string id, string direction, params KeyValuePair<string, object>[] properties)
        {
            JObject relationshipClass = CreateClassInstance(className, schemaName, id, properties);
            relationshipClass.Add("direction", direction);
            return relationshipClass;
        }

        public static JToken CreateJson(JObject instance, JObject relationshipInstance, JObject relatedInstance)
        {
            relationshipInstance.Add("relatedInstance", relatedInstance);

            var relationshipsArray = new JArray();
            relationshipsArray.Add(relationshipInstance);

            instance.Add("relationshipInstances", relationshipsArray);

            var json = new JObject();
            json.Add("instance", instance);

            return json;
        }
    }
}
