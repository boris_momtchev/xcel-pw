﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Xcel.Pw.BO;
using NLog;
using System.Diagnostics;

namespace Xcel.Pw.DAL
{
    public class WsgRepo : BaseRepo
    {
        // FIELDS
        static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        const int TIMEOUT = 10 * 60;

        // API
        public IEnumerable<PW_SAP_WBS_INCOMING_VW> Fetch_PW_SAP_WBS_INCOMING_VW()
        {
            using (var cn = Open_ProjectWise_Common())
            {
                string query = "SELECT * FROM [dbo].[PW_SAP_WBS_INCOMING_VW]";
                var result = cn.Query<PW_SAP_WBS_INCOMING_VW>(query, commandTimeout: TIMEOUT);
                return result;
            }
        }

        public IEnumerable<PW_WSG_TEMPLATE_MAN> Fetch_PW_WSG_TEMPLATE_MAN()
        {
            using (var cn = Open_ProjectWise_Common())
            {
                string query = "SELECT * FROM [dbo].[PW_WSG_TEMPLATE_MAN]";
                var result = cn.Query<PW_WSG_TEMPLATE_MAN>(query, commandTimeout: TIMEOUT);
                return result;
            }
        }

        public IEnumerable<PW_WSG_TARGET_JOB> Fetch_PW_WSG_TARGET_JOB()
        {
            using (var cn = Open_ProjectWise_Common())
            {
                string query = "SELECT * FROM [dbo].[PW_WSG_TARGET_JOB]";
                var result = cn.Query<PW_WSG_TARGET_JOB>(query, commandTimeout: TIMEOUT);
                return result;
            }
        }

        // BIZ
        public IEnumerable<PW_SAP_WBS_INCOMING_VW> Fetch_Incomplete_INCOMING()
        {
            using (var cn = Open_ProjectWise_Common())
            {
                string query =
@"
SELECT *
FROM [dbo].[PW_SAP_WBS_INCOMING_VW] i
WHERE  NOT EXISTS (SELECT *
                   FROM   [dbo].[PW_WSG_TARGET_JOB] j
                   WHERE  (i.TARGET_ROOT_TPLNR = j.TARGET_ROOT_TPLNR 
						  AND  
					        i.TARGET_NAME = j.TARGET_NAME
                          AND
                            j.JOB_STATUS = 'COMPLETE'    
                          ))";

                var result = cn.Query<PW_SAP_WBS_INCOMING_VW>(query, commandTimeout: TIMEOUT);
                return result;
            }
        }

        public IEnumerable<PW_WSG_TARGET_JOB> Fetch_Jobs()
        {
            var jobs = new List<PW_WSG_TARGET_JOB>();

            var incoming = Fetch_Incomplete_INCOMING();
            var templates = Fetch_PW_WSG_TEMPLATE_MAN();

            string missingTEMPLATE = string.Empty;

            // Create new tasks/jobs
            foreach (var inc in incoming)
            {
                var templ = templates.Where(x => x.TEMPLATE == inc.TEMPLATE).FirstOrDefault();
                if (templ == null)
                {
                    if (missingTEMPLATE != inc.TEMPLATE)
                    {
                        missingTEMPLATE = inc.TEMPLATE;
                        _logger.Warn("No TEMPLATE record exists in PW_WSG_TEMPLATE_MAN for: " + inc.TEMPLATE);
                    }
                    continue;
                }

                var job = new PW_WSG_TARGET_JOB();

                job.PW_DATASOURCE = templ.PW_DATASOURCE;
                job.TEMPLATE_GUID = templ.TEMPLATE_GUID;
                job.TEMPLATE_NAME = templ.TEMPLATE_NAME;

                job.TARGET_GUID = Guid.Empty;       // TODO: update to real TARGET GUID after WSG
                job.TARGET_ROOT_TPLNR = inc.TARGET_ROOT_TPLNR;
                job.TARGET_NAME = inc.TARGET_NAME;

                job.JOB_STATUS = "COMPLETE";        // Assume Success
                job.TimeStamp = DateTime.UtcNow;    // TODO: update to UtcNow/Now after WSG

                // DTO
                job.SCHEMA = templ.SCHEMA;
                job.PW_DATASOURCE_ID = "";          // Fetch_Projects populates it

                job.PARENT_FOLDER_NAME = inc.PARENT_FOLDER_NAME;
                job.TARGET_ROOT_NAME = templ.TARGET_ROOT_NAME;

                job.ATTRIBUTE_MATCH = templ.ATTRIBUTE_MATCH;
                job.ATTRIBUTE_VALUE = inc.ATTRIBUTE_VALUE;

                jobs.Add(job);
            }

            return jobs;
        }

        public void Save_Job(PW_WSG_TARGET_JOB job)
        {
            using (var cn = Open_ProjectWise_Common())
            {
                string query = string.Format(@"
INSERT INTO [dbo].[PW_WSG_TARGET_JOB]
  (PW_DATASOURCE, TEMPLATE_GUID, TEMPLATE_NAME, TARGET_GUID, TARGET_ROOT_TPLNR, TARGET_NAME, JOB_STATUS, [TimeStamp]) 
VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')", 
  job.PW_DATASOURCE, 
  job.TEMPLATE_GUID, 
  job.TEMPLATE_NAME, 
  job.TARGET_GUID, 
  job.TARGET_ROOT_TPLNR, 
  job.TARGET_NAME,
  job.JOB_STATUS,
  DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
);
                Debug.WriteLine("Insert query: " + query);
                int i = cn.Execute(query, commandTimeout: TIMEOUT);
                Debug.WriteLine("Inserted rows: " + i);
            }
        }


        // TEST
        //public string FetchFLOCNumber(string flocMnemonic, string flocName)
        //{
        //    using (var cn = Open_ProjectWise_Common())
        //    {
        //        string query =
        //            @" SELECT [FUNCTIONAL_LOCATION_NUMBER]
        //               FROM   [dbo].[SAP_SU_MUT_FLOC_MAPPER_VW]
        //               WHERE  [MNEMONIC] = @MNEMONIC
        //                 AND  [NAME] = @NAME
        //                 AND  [OBJECT_TYPE] = @OBJECT_TYPE
        //            ";

        //        var result = cn.Query<string>(query, new { MNEMONIC = flocMnemonic, NAME = flocName, OBJECT_TYPE = "SUBS" }, commandTimeout: TIMEOUT)
        //                       .SingleOrDefault();

        //        if (result == null) return null;
        //        return result.Trim();
        //    }
        //}

    }
}
