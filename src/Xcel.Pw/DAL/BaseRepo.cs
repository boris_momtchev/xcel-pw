﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xcel.Pw.DAL
{
    public abstract class BaseRepo
    {
        protected static IDbConnection Open_ProjectWise_Common()
        {
            IDbConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["ProjectWise_Common"].ConnectionString);
            cn.Open();
            return cn;
        }
    }
}
