﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xcel.Pw.SVC;

namespace Xcel.Pw
{
    class Program
    {
        // FIELDS
        static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        static void Main(string[] args)
        {
            _logger.Info("=> START:");

            var svc = new WsgSvc();
            svc.ProcessJobs();

            _logger.Info("<= END.\n");
        }
    }
}
